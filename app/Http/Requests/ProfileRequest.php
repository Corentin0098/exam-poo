<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $user = Auth::user();

        return [
            // $user->id récupère l'id de l'user connecté et vérifie qu'il ne modifie que son propre profil
            'email' => 'nullable|string|email|max:255|unique:user,email,' . $user->id,
            'password' => 'nullable|string|min:10'
        ];
    }
}
