<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login() {
        return view('login');
    }

    public function connectToProfile(LoginRequest $request)
    {
        // Valide les données du formulaire de connexion
        $data = $request->validated();

        // Convertie le champ 'username' en minuscules
        $data['username'] = strtolower($data['username']);

        // Connection de l'utilisateur en utilisant les données fournies si elles sont valides
        if (Auth::attempt($data)) {
            // Régénération de la session par sécurité (nouveau token) pour qu'un autre ne puise utilisé un ancien token lui permettant d'accéder à un compte qui ne lui appartient pas
            $request->session()->regenerate();

            return redirect()->route('profile', ['id' => Auth::id()])->with(
                'message', 'Bienvenue ' . ucfirst($data['username'])
            );
        }

        return redirect()->route('login')->withErrors([
            'Échec de l\'authentification'
        ]);
    }

    // LOGOUT
    public function logout(Request $request)
    {
        auth()->logout(); // Déconnecte l'utilisateur
        Session()->flush(); // / Supprime toutes les données stockées dans la session

        // Invalide la session de l'utilisateur, ne peut plus être utilisée après la déconnexion.
        $request->session()->invalidate();
        $request->session()->regenerateToken(); // régénère un nouveau jeton de session, par sécurité

        return redirect()->route('home')->with('message', 'Au revoir');
    }
}
