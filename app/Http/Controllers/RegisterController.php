<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Roles;

class RegisterController extends Controller
{
    public function register()
    {
        return view('register');
    }

    public function createRegister(RegisterRequest $request) {

        // Mettre le rôle "visiteur" à l'inscription
        $visiteurRole = Roles::where('name', 'visiteur')->first();

        // Vérifie si le rôle "visiteur" existe, sinon on le crée
        if (!$visiteurRole) {
            $visiteurRole = Roles::create(['name' => 'visiteur']);
        }

        // Données remplies par l'utilisateur lors de l'inscription et stockage dans la DB
        $user = User::create([
            'username' => strtolower($request->input('username')),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'lastlogin' => NULL,
            'image' => NULL,
            'role_id' => $visiteurRole->id
        ]);

        return redirect()->route('login')->with('success', 'Inscription réussie ! Vous pouvez maintenant vous connecter');
    }
}

