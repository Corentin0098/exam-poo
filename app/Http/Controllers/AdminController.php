<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangeRoleRequest;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Roles;

class AdminController extends Controller
{
    public function userList() {
        // Récupère tous les utilisateurs
        $users = User::all();
        return view('admin', [
            'users' => $users
        ]);
    }

    public function changeRole(ChangeRoleRequest $request, $id) {
        // Récupère l'utilisateur à mettre à jour
        $user = User::findOrFail($id);

        // Récupère le nom du rôle choisi depuis la requête
        $newRoleName = $request->input('role');

        // Vérifie si le rôle choisi est différent du rôle actuel de l'utilisateur
        if ($newRoleName !== $user->role->name) {
            // Vérifie si le rôle existe déjà dans la base de données
            $newRole = Roles::where('name', $newRoleName)->first();

            // Si le rôle n'existe pas, on le crée
            if (!$newRole) {
                $newRole = Roles::create(['name' => $newRoleName]);
            }

            // Assignation du nouveau rôle à l'utilisateur
            $user->role_id = $newRole->id;
            $user->save();

            return redirect()->route('admin')->with('success', 'Rôle de l\'utilisateur mis à jour avec succès');
        } else {
            return redirect()->route('admin')->withErrors(['Aucune modification de rôle n\'a été effectuée']);
        }
    }
}
