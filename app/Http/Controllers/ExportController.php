<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Response;
use App\Models\User;
use App\Models\Roles;
use Illuminate\Support\Facades\Auth;

class ExportController extends Controller
{
    public function exportJSON()
    {
        // Vérifie si l'utilisateur est authentifié
        if (Auth::check()) {
            // Récupère l'utilisateur actuellement authentifié
            $user = Auth::user();

            // Transforme les données de l'utilisateur en tableau pour l'export JSON
            $data = [
                'id' => $user->id,
                'username' => $user->username,
                'email' => $user->email,
                'role' => $user->role->name,
                'created_at' => Carbon::parse($user->created_at)->format('Y-m-d H:i:s'),
                'lastlogin' => $user->lastlogin,

            ];

            // Convertit les données de l'utilisateur ($data) en format JSON
            $jsonContent = json_encode($data);

            // Crée une réponse HTTP avec le contenu JSON
            return Response::make($jsonContent, 200, [
                'Content-Type' => 'application/json',
                'Content-Disposition' => 'attachment; filename="export.json"',
            ]);
        }
            return redirect()->route('login')->withErrors([
                'Veuillez-vous connecter!'
            ]);
    }

    public function exportCSV()
    {
        // Vérifiez si l'utilisateur est authentifié
        if (Auth::check()) {
            // Récupère l'utilisateur actuellement authentifié
            $user = Auth::user();

            // Générez le contenu CSV avec les données de l'utilisateur
            $csvContent = "ID, Username, Email, Role, Created_at ,Last Login\n";
            $csvContent .= "{$user->id}, {$user->username}, {$user->email}, {$user->role->name},{$user->created_at}, {$user->lastlogin}";

            // Crée une réponse HTTP avec le contenu CSV
            return Response::make($csvContent, 200, [
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename="export.csv"',
            ]);
        }
            return redirect()->route('login')->withErrors([
                'Veuillez-vous connecter!'
            ]);
    }
}
