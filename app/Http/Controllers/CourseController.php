<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Course;

class CourseController extends Controller
{
    public function courses() {
        // Récupère toute la liste des cours
        $courses = Course::all();
        return view('courses', [
            'courses' => $courses
        ]);
    }
}
