<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function profile($id) {
        // Récupère l'utilisateur actuellement authentifié
        $user = Auth::user();

        // Vérifie si l'ID de l'utilisateur correspond à celui de l'utilisateur authentifié
        if ($id != $user->id) {
            return back()->withErrors(['Vous n\'avez pas la permission d\'accéder à ce profil']);
        }

        return view('profile', [
            'user' => $user
        ]);
    }

    public function updateProfile(ProfileRequest $request, $id)
    {
        // Récupère l'utilisateur actuellement authentifié
        $user = Auth::user();

        // Vérifie si l'ID de l'utilisateur passé en paramètre correspond à l'ID de l'utilisateur authentifié
        if ($id != $user->id) {
            return back()->withErrors(['Vous n\'avez pas la permission de modifier ce profil']);
        }

        // Vérifie si une nouvelle adresse e-mail est fournie et qu'elle est différente de l'ancienne
        if ($request->filled('email') && $request->input('email') !== $user->email) {
            $user->email = $request->input('email');
        }

        // Vérifier si un nouveau mot de passe est fourni
        if ($request->filled('password')) {
            $user->password = Hash::make($request->input('password'));
        }

        // Sauvegarder l'utilisateur uniquement s'il y a des changements (isDirty() méthode qui vérifie ça (Eloquent)
        if ($user->isDirty()) {
            $user->save();
            return back()->with('message', 'Modification réussie');
        }

        return back()->withErrors(['Aucune modification n\'a été effectuée']);
    }
}
