<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        // Vérifie si l'utilisateur est authentifié et est un admin
        if (auth()->check() && auth()->user()->role->name === 'admin' ) {
            return $next($request);
        }

        // Redirection vers la page d'accueil si l'utilisateur n'est pas admin
        return redirect()->route('home')->withErrors(['Vous n\'êtes pas administrateur']);
    }
}
