<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;

class UpdateLastLoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        // Exécute la requête
        $response = $next($request);

        // Vérifie si l'utilisateur est connecté
        if (Auth::check()) {
            // Mise à jour de la colonne lastlogin de l'utilisateur actuellement connecté
            Auth::user()->update(['lastlogin' => now()]);
        }

        return $response;
    }
}
