<?php

namespace App\Console\Commands;

use App\Models\Roles;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $isValid = false;

        while (!$isValid) {
            // Demande de fournir les infos
            $username = $this->ask('Enter the administrator\'s name (min:4)');
            $email = $this->ask('Enter the administrator\'s email');
            $password = $this->secret('Enter the administrator\'s password (min:10)');

            // Convertir l'username en minuscules
            $username = strtolower($username);

            // Règles de validation
            $validator = Validator::make([
                'username' => $username,
                'email' => $email,
                'password' => $password,
            ], [
                'username' => 'required|string|min:4|max:20|unique:user',
                'email' => 'required|string|email|max:255|unique:user',
                'password' => 'required|string|min:10',
            ]);

            // Si la validation a échoué
            if ($validator->fails()) {
                $this->error('Validation failed. Please correct the following errors:');
                foreach ($validator->errors()->all() as $error) {
                    $this->error('- ' . $error);
                }
            } else {
                $isValid = true; // Si toutes les données sont valides, je sors de la boucle
            }
        }

        // Obtenez le rôle "admin"
        $adminRole = Roles::where('name', 'admin')->first();

        if (!$adminRole) {
            $adminRole = Roles::create(['name' => 'admin']);
        }

        // Création de l'admin
        $user = User::create([
            'username' => $username,
            'email' => $email,
            'password' => Hash::make($password),
            'lastlogin' => NULL,
            'image' => NULL,
            'role_id' => $adminRole->id
        ]);

        $this->info('The administrator has been created !');
    }
}
