<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class InstallProject extends Command
{
    protected $signature = 'install:project';

    protected $description = 'Install the project and update the .env file';

    public function handle()
    {
        // Copie le fichier .env.example en tant que .env
        File::copy('.env.example', '.env');

        // Génère une nouvelle APP_KEY
        $this->call('key:generate');

        // Demander les informations du serveur
        $dbHost = $this->ask('Enter the database host (Enter for default)', '127.0.0.1');
        $dbPort = $this->ask('Enter the database port (Enter for default)', '3306');
        $dbDatabase = $this->ask('Enter the name of the database (Exemple: web_poo)');
        $dbUsername = $this->ask('Enter the database username (Enter for default)', 'root');
        $dbPassword = $this->secret('Enter the database password (default: empty)');

        // Charge le contenu du fichier .env
        $envContents = File::get('.env');

        // Met à jour le fichier .env avec les informations récupéré
        // (preg_replace) Recherche et remplace les lignes spécifiques dans le contenu du fichier .env
        $envContents = preg_replace([
            '/APP_DEBUG=(.*)/',
            '/DB_HOST=(.*)/',
            '/DB_PORT=(.*)/',
            '/DB_DATABASE=(.*)/',
            '/DB_USERNAME=(.*)/',
            '/DB_PASSWORD=(.*)/'
        ], [
            'APP_DEBUG=false',
            'DB_HOST=' . $dbHost,
            'DB_PORT=' . $dbPort,
            'DB_DATABASE=' . $dbDatabase,
            'DB_USERNAME=' . $dbUsername,
            'DB_PASSWORD=' . $dbPassword
        ], $envContents);

        // Met les modifications dans le fichier .env
        File::put('.env', $envContents);

        $this->info('Laravel project has been installed !');
    }
}
