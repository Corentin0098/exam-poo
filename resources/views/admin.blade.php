@extends('layouts.master')

@section('title')
    Admin
@endsection

@section('content')

    {{-- Message success --}}
    @if(session('success'))
        <div class="alert alert-success text-center w-50 mx-auto mt-5">
            {{ session('success') }}
        </div>
    @endif

    {{-- Tous les messages d'erreurs --}}
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger text-center w-50 mx-auto mt-3">
                {{ $error }}
            </div>
        @endforeach
    @endif

    <table class="table container w-75 mt-5 text-center align-middle">
        <tr><th>Identifiant</th><th>Email</th><th>Date de création</th><th>Dernière visite</th><th>Rôle</th></tr>

        @foreach($users as $user)
        <tr>
            <td>{{ ucfirst($user->username) }}</td>
            <td>{{ $user->email }}</td>
            <td>
                @php
                    $creationDate = \Carbon\Carbon::parse($user->created_at);
                @endphp
                {{ $creationDate->format('d/m/Y H:i:s') }}
            </td>
            <td>
                @if ($user->lastlogin === null)
                    Jamais
                @else
                    @php
                        $lastLogin = \Carbon\Carbon::parse($user->lastlogin);
                    @endphp
                    {{ $lastLogin->format('d/m/Y H:i:s') }}
                @endif
            </td>
            <td>
                @if ($user->role->name === 'admin')
                    {{ ucfirst($user->role->name) }}
                @else
                    <form method="POST" action="{{ route('changeRole', $user->id) }}">
                        @csrf
                        <select class="select" name="role">
                            <option value="visiteur" @if($user->role->name === 'visiteur') selected @endif>Visiteur</option>
                            <option value="etudiant" @if($user->role->name === 'etudiant') selected @endif>Étudiant</option>
                            <option value="professeur" @if($user->role->name === 'professeur') selected @endif>Professeur</option>
                        </select>
                        <button type="submit" class="btn btn-primary">Mettre à jour</button>
                    </form>
                @endif
            </td>
        </tr>
        @endforeach
    </table>

@endsection
