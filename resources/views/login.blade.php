@extends('layouts.master')

@section('title')
    Connexion
@endsection

@section('content')

    {{-- Message success si le compte est créer --}}
    @if(session('success'))
        <div class="alert alert-success text-center w-50 mx-auto mt-5">
            {{ session('success') }}
        </div>
    @endif

    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger text-center w-50 mx-auto mt-3">
                {{ $error }}
            </div>
        @endforeach
    @endif

    <section>
        <div class="px-4 py-5 px-md-5 text-center text-lg-start">
            <div class="container">
                <div class="row gx-lg-5 align-items-center justify-content-center">
                    <div class="col-lg-6 mb-5 mb-lg-0">
                        <div class="card">
                            <div class="card-body py-5 px-md-5">
                                <form method="POST" action="{{ route('LoginController') }}">
                                    @csrf
                                    <!-- Identifiant input -->
                                    <div class="form-outline mb-4">
                                        <label class="form-label" for="username">Identifiant :</label>
                                        <input type="text" id="username" name="username" class="form-control" />
                                    </div>

                                    <!-- Password input -->
                                    <div class="form-outline mb-4">
                                        <label class="form-label" for="password">Mot de passe :</label>
                                        <input type="password" id="password" name="password" class="form-control" />
                                    </div>

                                    <!-- Submit button -->
                                    <button type="submit" class="btn btn-primary btn-block mb-4">
                                        Connexion
                                    </button>

                                    <!-- Register button -->
                                    <a href="{{ route('register') }}" class="btn btn-success btn-block mb-4 text-white text-decoration-none">
                                        Inscription
                                    </a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
