<!-- Start Header Area -->
<header class="bg-white">
    <nav class="nav nav-tabs">
        <div class="container d-flex justify-content-evenly py-3">
            <a class="nav-link text-dark" href="{{ route('home') }}">Accueil</a>
            <a class="nav-link text-dark" href="{{ route('courses') }}">Liste des cours</a>

            @auth
                <a class="nav-link text-dark" href="{{ route('profile', ['id' => Auth::user()->id]) }}">Profil</a>
            @else
                <a class="nav-link text-dark" href="{{ route('login') }}">Profil</a>
            @endauth

            <a class="nav-link text-dark" href="{{ route('admin') }}">Admin</a>

            @guest()
            <a class="nav-link text-dark" href="{{ route('login') }}">Login</a>
            @else
            <a class="nav-link text-danger" href="{{ route('logout') }}">Logout</a>
            @endguest
        </div>
    </nav>
</header>
<!-- End Header Area -->


