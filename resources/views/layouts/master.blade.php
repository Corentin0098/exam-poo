<!DOCTYPE html>
<html lang="fr">

<head>
    <!-- Author Meta -->
    <meta name="author" content="Corentin">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>POO & Framework |@yield('title')</title>
    <!-- CDN Boostrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>

<body style="background-color: hsl(0, 0%, 96%)">

    <div id="app">
        @include('layouts.header')
        @yield('content')
        @include('layouts.footer')
    </div>

{{-- Script JavaScript --}}
{{-- CDN Boostrap --}}
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>

</body>
</html>
