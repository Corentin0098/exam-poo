@extends('layouts.master')

@section('title')
    Cours
@endsection

@section('content')

    <h1 class="text-center my-5">Liste des cours</h1>

    <table class="table container w-50 border bg-white">
        <thead>
        <tr>
            <th>Nom du cours</th>
            <th class="text-end">Code</th>
        </tr>
        </thead>
        <tbody>
            @foreach($courses as $course)
                <tr>
                    <td>{{ $course->name }}</td>
                    <td class="text-end">{{ $course->code }}</td>

                    @if(auth()->user() && auth()->user()->role === 'etudiant')
                        <td class="d-flex justify-content-end">
                            <button type="submit" class="btn btn-outline-success">
                                S'inscrire
                            </button>
                        </td>
                    @elseif(auth()->user() && auth()->user()->role === 'professeur')
                        <td class="d-flex justify-content-end">
                            <button type="submit" class="btn btn-outline-success">
                                S'assigné
                            </button>
                        </td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>

@endsection
