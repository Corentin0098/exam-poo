@extends('layouts.master')

@section('title')
    Accueil
@endsection

@section('content')

    @if(session('message'))
        <div class="alert alert-primary text-center w-50 mx-auto mt-3">
            {{ session('message') }}
        </div>
    @endif

    {{-- Tous les messages d'erreurs --}}
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger text-center w-50 mx-auto mt-3">
                {{ $error }}
            </div>
        @endforeach
    @endif

@endsection
