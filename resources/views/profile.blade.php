@extends('layouts.master')

@section('title')
    Profile
@endsection

@section('content')

    @if(session('message'))
        <div class="alert alert-success text-center w-50 mx-auto mt-3">
            {{ session('message') }}
        </div>
    @endif

    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger text-center w-50 mx-auto mt-3">
                {{ $error }}
            </div>
        @endforeach
    @endif

    <h1 class="text-center my-5">Profile</h1>
    <table class="table container w-50 mt-3 border">
        <tbody>
        <tr>
            <th>Identifiant :</th>
            <td>{{ ucfirst($user->username) }}</td>
        </tr>
        <tr>
            <th>Email :</th>
            <td>{{ $user->email }}</td>
        </tr>
        <tr>
            <th>Création :</th>
            <td>
                @php
                    $creationDate = \Carbon\Carbon::parse($user->created_at);
                @endphp
                {{ $creationDate->format('d/m/Y H:i:s') }}
            </td>
        </tr>
        <tr>
            <th>Dernière visite :</th>
            <td>
                @php
                    $lastLogin = \Carbon\Carbon::parse($user->lastlogin);
                @endphp
                {{ $lastLogin->format('d/m/Y H:i:s') }}
            </td>
        </tr>
        <tr>
            <th>Rôle :</th>
            <td>{{ ucfirst($user->role->name) }}</td>
        </tr>
        </tbody>
    </table>

    {{-- Buttons d'export des données du profil --}}
    <div class="d-flex justify-content-center mt-5">
        <form action="{{ route('export.json') }}" method="GET">
            <button type="submit" class="btn btn-outline-success me-2">
                <i class="bi bi-download pe-2"></i> Export JSON
            </button>
        </form>

        <form action="{{ route('export.csv') }}" method="GET">
            <button type="submit" class="btn btn-outline-success">
                <i class="bi bi-download pe-2"></i> Export CSV
            </button>
        </form>
    </div>


    {{-- Formulaire de mise à jour du compte --}}
    <div class="container w-50 my-5">
        <h3>
            <a href="#update" data-bs-toggle="collapse" role="button" class="text-decoration-none btn btn-primary dropdown-toggle">
                Mettre à jour le profil
            </a>
        </h3>
        <div class="collapse" id="update">
            <form method="POST" action="{{ route('updateProfile', ['id' => $user->id]) }}">
                @csrf
                <div class="mb-3">
                    <label for="email" class="form-label fw-bold mt-3">Email :</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}">
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label fw-bold">Mot de passe :</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="•••••••••••••••">
                </div>
                <button type="submit" class="btn btn-outline-primary">Envoyer</button>
            </form>
        </div>
    </div>
@endsection
