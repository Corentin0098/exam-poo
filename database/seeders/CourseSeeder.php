<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Course;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Course::insert([
            [
                'id' => 1,
                'name' => 'Base des réseaux',
                'code' => '1351',
            ],
            [
                'id' => 2,
                'name' => 'Environnement et technologies du web',
                'code' => '1352',
            ],
            [
                'id' => 3,
                'name' => 'SGBD (Système de gestion de bases de données)',
                'code' => '1353',
            ],
            [
                'id' => 4,
                'name' => 'Création de sites web statiques',
                'code' => '1354',
            ],
            [
                'id' => 5,
                'name' => 'Approche Design',
                'code' => '1355',
            ],
            [
                'id' => 6,
                'name' => 'CMS - niveau 1',
                'code' => '1356',
            ],
            [
                'id' => 7,
                'name' => 'Initiation à la programmation',
                'code' => '1357',
            ],
            [
                'id' => 8,
                'name' => 'Activités professionnelles de formation',
                'code' => '1358',
            ],
            [
                'id' => 9,
                'name' => 'Scripts clients',
                'code' => '1359',
            ],
            [
                'id' => 10,
                'name' => 'Scripts serveurs',
                'code' => '1360',
            ],
            [
                'id' => 11,
                'name' => 'Framework et POO côté Serveur',
                'code' => '1361',
            ],
            [
                'id' => 12,
                'name' => 'Projet Web dynamique',
                'code' => '1362',
            ],
            [
                'id' => 13,
                'name' => 'Veille technologique',
                'code' => '1363',
            ],
            [
                'id' => 14,
                'name' => 'Epreuve intégrée',
                'code' => '1364',
            ],
            [
                'id' => 15,
                'name' => 'Anglais UE1',
                'code' => '1783',
            ],
            [
                'id' => 16,
                'name' => 'Anglais UE2',
                'code' => '1784',
            ],
            [
                'id' => 17,
                'name' => 'Initiation aux bases de données',
                'code' => '1440',
            ],
            [
                'id' => 18,
                'name' => 'Principes algorithmiques et programmation',
                'code' => '1442',
            ],
            [
                'id' => 19,
                'name' => 'Programmation orientée objet',
                'code' => '1443',
            ],
            [
                'id' => 20,
                'name' => 'Web : principes de base',
                'code' => '1444',
            ],
            [
                'id' => 21,
                'name' => 'Techniques de gestion de projet',
                'code' => '1448',
            ],
            [
                'id' => 22,
                'name' => 'Principes d’analyse informatique',
                'code' => '1449',
            ],
            [
                'id' => 23,
                'name' => 'Eléments de statistique',
                'code' => '1755',
            ],
            [
                'id' => 24,
                'name' => 'Structure des ordinateurs',
                'code' => '1808',
            ],
            [
                'id' => 25,
                'name' => 'Gestion et exploitation de bases de données',
                'code' => '1811',
            ],
            [
                'id' => 26,
                'name' => 'Mathématiques appliquées à l’informatique',
                'code' => '1807',
            ],
            [
                'id' => 27,
                'name' => 'Bases des réseaux',
                'code' => '1323',
            ],
            [
                'id' => 28,
                'name' => 'Projet d’analyse et de conception',
                'code' => '1450',
            ],
            [
                'id' => 29,
                'name' => 'Information et communication professionnelle',
                'code' => '1754',
            ],
            [
                'id' => 30,
                'name' => 'Produits logiciels de gestion intégrés',
                'code' => '1438',
            ],
            [
                'id' => 31,
                'name' => 'Administration, gestion et sécurisation des réseaux',
                'code' => '1439',
            ],
            [
                'id' => 32,
                'name' => 'Projet de développement SGBD',
                'code' => '1446',
            ],
            [
                'id' => 33,
                'name' => 'Stage d’intégration professionnelle',
                'code' => '1451',
            ],
            [
                'id' => 34,
                'name' => 'Projet d’intégration de développement',
                'code' => '1447',
            ],
            [
                'id' => 35,
                'name' => 'Activités professionnelles de formation',
                'code' => '1452',
            ],
            [
                'id' => 36,
                'name' => 'Epreuve intégrée de la section',
                'code' => '1453',
            ],
        ]);
    }
}
