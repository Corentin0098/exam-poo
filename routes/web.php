<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::namespace('App\Http\Controllers')->group(function () {

    // Main page
    Route::get('/', 'HomeController@home')->name('home');

    // Courses page
    Route::get('courses', 'CourseController@courses')->name('courses');

    // Register
    Route::get('register', 'RegisterController@register')->name('register');
    Route::post('register', 'RegisterController@createRegister')->name('createRegister');

    // Login
    Route::get('login', 'LoginController@login')->name('login');
    Route::post('login', 'LoginController@connectToProfile')->name('LoginController');

    // Profil
    Route::middleware(['auth'])->group(function () {
        Route::get('profile/{id}', 'ProfileController@profile')->name('profile');
        Route::post('updateProfile/{id}', 'ProfileController@updateProfile')->name('updateProfile');
    });


    // Admin
    Route::middleware(['admin'])->group(function () {
        Route::get('admin', 'AdminController@userList')->name('admin');
    });
    Route::post('changeRole/{id}', 'AdminController@changeRole')->name('changeRole');


    // Logout
    Route::get('logout', 'LoginController@logout')->name('logout');


    // Export datas
    Route::get('export/json', 'ExportController@exportJSON')->name('export.json');
    Route::get('export/csv', 'ExportController@exportCSV')->name('export.csv');

});
